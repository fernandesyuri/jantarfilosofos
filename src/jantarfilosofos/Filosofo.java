package jantarfilosofos;

public class Filosofo implements Runnable {

    private final Object garfoEsquerda;
    private final Object garfoDireita;

    public Filosofo(Object garfoEsquerda, Object garfoDireita) {
        this.garfoEsquerda = garfoEsquerda;
        this.garfoDireita = garfoDireita;
    }

    @Override
    public void run() {
        try {
            while (true) {
                // Pensando
                acao(": Pensando");
                synchronized (garfoEsquerda) {
                    acao(": Pegou o garfo da esquerda");
                    synchronized (garfoDireita) {
                        // eating
                        acao(": Pegou o garfo da direita - comendo");
                    }
                    // Voltando a pensar
                    System.out.println(Thread.currentThread().getName() + ": Voltando a pensar");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void acao(String action) throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + action);
        Thread.sleep(((int) (Math.random() * 5000)));
    }
}
