package jantarfilosofos;

/**
 *
 * @author Yuri Fernandes
 */
public class JantarFilosofos {

    public static void main(String[] args) {

        final Filosofo[] filosofos = new Filosofo[5];
        Object[] garfos = new Object[filosofos.length];

        for (int i = 0; i < garfos.length; i++) {
            garfos[i] = new Object();
        }

        for (int i = 0; i < filosofos.length; i++) {
            Object garfoEsquerda = garfos[i];
            Object garfoDireita = garfos[(i + 1) % garfos.length];

            if (i == filosofos.length - 1) {
                // O ultimo filosofo pega o hashi da direita primeiro, removendo a condição circular e a possibilidade de deadlock
                filosofos[i] = new Filosofo(garfoDireita, garfoEsquerda);
            } else {
                filosofos[i] = new Filosofo(garfoEsquerda, garfoDireita);
            }

            Thread t = new Thread(filosofos[i], "Filósofo " + (i + 1));
            t.start();
        }
    }

}
